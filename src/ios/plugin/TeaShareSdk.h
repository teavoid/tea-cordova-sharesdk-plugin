//
//  TeaShareSdk.h
//  Ryan
//
//  Created by Ryan on 2015-07-04.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <UIKit/UIKit.h>

@interface TeaShareSdk : CDVPlugin


// Instance Method

  - (void)teashare:(CDVInvokedUrlCommand*)command;
  - (void)initShareSdk;
  - (void)loginWithWeiXin:(CDVInvokedUrlCommand*)command;
  - (void)loginWithQQ:(CDVInvokedUrlCommand*)command;
  - (void)loginWithWeiBo:(CDVInvokedUrlCommand*)command;
@end
