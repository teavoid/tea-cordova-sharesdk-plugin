
#import "TeaShareSdk.h"
#import <ShareSDK/ShareSDK.h>
#import <Cordova/CDVPlugin.h>
#import <QuartzCore/QuartzCore.h>

//第三方平台的SDK头文件，根据需要的平台导入。
//以下分别对应微信、新浪微博、腾讯微博、人人、易信
#import "WXApi.h"
#import "WeiboSDK.h"
#import "WeiboApi.h"

//以下是腾讯QQ和QQ空间
#import <TencentOpenAPI/QQApi.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentOAuth.h>

@implementation TeaShareSdk


- (void)teashare:(CDVInvokedUrlCommand *)command {

    [self initShareSdk];

    NSString* title = [command.arguments objectAtIndex:0];
    NSString* fxcontent = [command.arguments objectAtIndex:1];
    NSString* url = [command.arguments objectAtIndex:2];
    NSString* imageurl = [command.arguments objectAtIndex:3];

    
    //构造分享内容
    id<ISSContent> publishContent = [ShareSDK content:fxcontent
                                       defaultContent:fxcontent
                                                image:[ShareSDK imageWithUrl:imageurl]
                                                title:title
                                                  url:url
                                          description:fxcontent
                                            mediaType:SSPublishContentMediaTypeNews];
    //创建弹出菜单容器
    id<ISSContainer> container = [ShareSDK container];
   // [container setIPadContainerWithView:sender arrowDirect:UIPopoverArrowDirectionUp];

    CDVPluginResult* pluginResult = nil;

    //弹出分享菜单
    [ShareSDK showShareActionSheet:container
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions:nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_SUC", @"分享成功"));
                                    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"1"];
                                     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(NSLocalizedString(@"TEXT_ShARE_FAI", @"分享失败,错误码:%d,错误描述:%@"), [error errorCode], [error errorDescription]);
                                    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"2"];
                                     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                }
                                else if  (state == SSResponseStateCancel) {
                                    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"3"];
                                     [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

                                }
                            }];


}

- (void)initShareSdk {
    [ShareSDK registerApp:@"7e124d172f87"];//字符串api20为您的ShareSDK的AppKey
    
    //添加新浪微博应用 注册网址 http://open.weibo.com
    [ShareSDK connectSinaWeiboWithAppKey:@"568898243"
                               appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                             redirectUri:@"http://www.sharesdk.cn"];
    //当使用新浪微博客户端分享的时候需要按照下面的方法来初始化新浪的平台 （注意：2个方法只用写其中一个就可以）
    [ShareSDK  connectSinaWeiboWithAppKey:@"568898243"
                                appSecret:@"38a4f8204cc784f81f9f0daaf31e02e3"
                              redirectUri:@"http://www.sharesdk.cn"
                              weiboSDKCls:[WeiboSDK class]];
    
    //添加腾讯微博应用 注册网址 http://dev.t.qq.com
    [ShareSDK connectTencentWeiboWithAppKey:@"801307650"
                                  appSecret:@"ae36f4ee3946e1cbb98d6965b0b2ff5c"
                                redirectUri:@"http://www.sharesdk.cn"
                                   wbApiCls:[WeiboApi class]];
    
    //添加QQ空间应用  注册网址  http://connect.qq.com/intro/login/
    [ShareSDK connectQZoneWithAppKey:@"100371282"
                           appSecret:@"aed9b0303e3ed1e27bae87c33761161d"
                   qqApiInterfaceCls:[QQApiInterface class]
                     tencentOAuthCls:[TencentOAuth class]];
    
    //添加QQ应用  注册网址  http://mobile.qq.com/api/
    [ShareSDK connectQQWithQZoneAppKey:@"100371282"
                     qqApiInterfaceCls:[QQApiInterface class]
                       tencentOAuthCls:[TencentOAuth class]];
    
    //添加微信应用  http://open.weixin.qq.com
    /*
    [ShareSDK connectWeChatWithAppId:@"wxa6a5bd2f1115b0d1"
                           appSecret:@"e3adcec04c2e7c7c8ae8e82c1bddea27"
                           wechatCls:[WXApi class]];
    */
    [ShareSDK connectWeChatWithAppId:@"wx4868b35061f87885"
                           appSecret:@"64020361b8ec4c99936c0e3999a9f249"
                           wechatCls:[WXApi class]];

    //连接邮件
    [ShareSDK connectSMS];
}

- (void)handleOpenURL:(NSNotification*)notification {
    NSURL* url = [notification object];
    if ([url isKindOfClass:[NSURL class]]) {
        [ShareSDK handleOpenURL:url
              sourceApplication:@""
                     annotation:nil
                     wxDelegate:self];

    }
}

- (void)pluginInitialize {
    [super pluginInitialize];
    [self initShareSdk];
}

- (void)loginWithQQ:(CDVInvokedUrlCommand*)command {
    ShareType type = ShareTypeQQSpace;

    //设置授权选项
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                             allowCallback:YES
                                                             authViewStyle:SSAuthViewStyleFullScreenPopup
                                                              viewDelegate:nil
                                                   authManagerViewDelegate:nil];

    [ShareSDK getUserInfoWithType:type
                authOptions:authOptions
                result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {

                if (result)
                                                   {
                                                        NSLog(@"ShareTypeQQ登录成功");
                                                       if ([userInfo profileImage])
                                                       {

                                                       }

                                                       switch (type)
                                                       {
                                                           case ShareTypeWeixiSession:
                                                               //微信
                                                               //[self fillWeixinUser:userInfo];
                                                               break;
                                                           default:
                                                               break;
                                                       }

                                                       NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];

                                                       NSArray *keys = [[userInfo sourceData] allKeys];
                                                       for (int i = 0; i < [keys count]; i++)
                                                       {
                                                           NSString *keyName = [keys objectAtIndex:i];
                                                           id value = [[userInfo sourceData] objectForKey:keyName];
                                                           if (![value isKindOfClass:[NSString class]])
                                                           {
                                                               if ([value respondsToSelector:@selector(stringValue)])
                                                               {
                                                                   value = [value stringValue];
                                                               }
                                                               else
                                                               {
                                                                   value = @"";
                                                               }
                                                           }
                                                           NSLog(@"key is %@, value is %@", keyName, value);
                                                           [dict setObject:value forKey:keyName];
                                                       }

                                                       [dict setObject:@"1" forKey:@"result"];
                                                       CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
                                                       [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                                   }
                                                   else
                                                   {
                                                        NSLog(@"ShareTypeQQ登录失败");
                                                        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
                                                        [dict setObject:@"2" forKey:@"result"];
                                                        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
                                                        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

                                                   }


    }];
}

- (void)loginWithWeiBo:(CDVInvokedUrlCommand*)command {
    ShareType type = ShareTypeSinaWeibo;

    //设置授权选项
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                             allowCallback:YES
                                                             authViewStyle:SSAuthViewStyleFullScreenPopup
                                                              viewDelegate:nil
                                                   authManagerViewDelegate:nil];

    [ShareSDK getUserInfoWithType:type
                authOptions:authOptions
                result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {

                if (result)
                                                   {
                                                        NSLog(@"ShareTypeSinaWeibo登录成功");
                                                       if ([userInfo profileImage])
                                                       {

                                                       }

                                                       switch (type)
                                                       {
                                                           case ShareTypeWeixiSession:
                                                               //微信
                                                               //[self fillWeixinUser:userInfo];
                                                               break;
                                                           default:
                                                               break;
                                                       }

                                                       NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];

                                                       NSArray *keys = [[userInfo sourceData] allKeys];
                                                       for (int i = 0; i < [keys count]; i++)
                                                       {
                                                           NSString *keyName = [keys objectAtIndex:i];
                                                           id value = [[userInfo sourceData] objectForKey:keyName];
                                                           if (![value isKindOfClass:[NSString class]])
                                                           {
                                                               if ([value respondsToSelector:@selector(stringValue)])
                                                               {
                                                                   value = [value stringValue];
                                                               }
                                                               else
                                                               {
                                                                   value = @"";
                                                               }
                                                           }
                                                           NSLog(@"key is %@, value is %@", keyName, value);
                                                           [dict setObject:value forKey:keyName];
                                                       }

                                                       [dict setObject:@"1" forKey:@"result"];
                                                       CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
                                                       [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                                   }
                                                   else
                                                   {
                                                        NSLog(@"ShareTypeSinaWeibo登录失败");
                                                        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
                                                        [dict setObject:@"2" forKey:@"result"];
                                                        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
                                                        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

                                                   }


    }];
}

- (void)loginWithWeiXin:(CDVInvokedUrlCommand*)command {
    ShareType type = ShareTypeWeixiSession;

    //设置授权选项
    id<ISSAuthOptions> authOptions = [ShareSDK authOptionsWithAutoAuth:YES
                                                             allowCallback:YES
                                                             authViewStyle:SSAuthViewStyleFullScreenPopup
                                                              viewDelegate:nil
                                                   authManagerViewDelegate:nil];

    [ShareSDK getUserInfoWithType:type
                authOptions:authOptions
                result:^(BOOL result, id<ISSPlatformUser> userInfo, id<ICMErrorInfo> error) {

                if (result)
                                                   {
                                                        NSLog(@"微信登录成功");
                                                       if ([userInfo profileImage])
                                                       {

                                                       }

                                                       switch (type)
                                                       {
                                                           case ShareTypeWeixiSession:
                                                               //微信
                                                               //[self fillWeixinUser:userInfo];
                                                               break;
                                                           default:
                                                               break;
                                                       }

                                                       NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];

                                                       NSArray *keys = [[userInfo sourceData] allKeys];
                                                       for (int i = 0; i < [keys count]; i++)
                                                       {
                                                           NSString *keyName = [keys objectAtIndex:i];
                                                           id value = [[userInfo sourceData] objectForKey:keyName];
                                                           if (![value isKindOfClass:[NSString class]])
                                                           {
                                                               if ([value respondsToSelector:@selector(stringValue)])
                                                               {
                                                                   value = [value stringValue];
                                                               }
                                                               else
                                                               {
                                                                   value = @"";
                                                               }
                                                           }
                                                           NSLog(@"key is %@, value is %@", keyName, value);
                                                           [dict setObject:value forKey:keyName];
                                                       }

                                                       [dict setObject:@"1" forKey:@"result"];
                                                       CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
                                                       [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                                   }
                                                   else
                                                   {
                                                        NSLog(@"微信登录失败");
                                                        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
                                                        [dict setObject:@"2" forKey:@"result"];
                                                        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dict];
                                                        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];

                                                   }


    }];
}
@end

